const app = require('http').createServer(serverListener)
const io = require('socket.io')(app);
const fs = require('fs');
const logger = require('winston');

const port = process.env.PORT || 8081;
let connectedUsersCount = 0;

//
// SETUP
//
logger.cli();
logger.default.transports.console.timestamp = true;

logger.info(`Server starting on port ${port}`)
app.listen(port);

//
// LISTENERS
//
function serverListener(req, res) {
  res.writeHead(200);
  res.end();

  // if (req.url == '/health') {
  //   res.writeHead(200);
  //   res.end();
  // } else {

    // fs.readFile(__dirname + '/index.html', function(err, data) {
    //   if (err) {
    //     res.writeHead(500);
    //     return res.end('Error loading index.html');
    //   }
    //
    //   res.writeHead(200);
    //   res.end(data);
    // });
  // }
}

io.on('connection', (socket) => {
  connectedUsersCount++;

  socket.on('game-channel', function(data) {
    socket.emit('game-channel', 'server result');
  });

  socket.on('disconnect', function(data) {
    connectedUsersCount--;
  });
});

//
// FUNCTIONS
//
setInterval(function() {
  logger.info(`connected users: ${connectedUsersCount}`);
}, 10 * 1000);
